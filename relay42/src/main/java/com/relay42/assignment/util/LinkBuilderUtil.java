package com.relay42.assignment.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.relay42.assignment.controller.SensorController;
import com.relay42.assignment.model.Sensor;

/**
 * @author Ibrahim AbdelHamid
 * 
 *         implemented as there is a current bug in the hateoas constructing
 *         with added params
 */
@Component
public class LinkBuilderUtil {

	/**
	 * @param clazz
	 * @param pageable
	 * @param params
	 * @param assembler
	 * @return Link
	 */
	@SuppressWarnings("unchecked")
	public static Link buildSelfLink(Class<?> clazz, Pageable pageable, String[] params,
			PagedResourcesAssembler<?> assembler) {

		Link link = null;
		if (clazz.isAssignableFrom(SensorController.class)) {
			link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SensorController.class)
					.getSensorsData(pageable, params, (PagedResourcesAssembler<Sensor>) assembler)).withSelfRel();
		}
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(link.expand().getHref());
		new HateoasPageableHandlerMethodArgumentResolver().enhance(builder, null, pageable);
		return new Link(builder.build().toString());
	}
}
