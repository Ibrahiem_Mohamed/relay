package com.relay42.assignment.repository.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.relay42.assignment.model.security.User;

/**
 * @author Ibrahim AbdelHamid
 *
 */


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * @param username
     * @return User 
     */
    @Query("SELECT DISTINCT user FROM User user " +
            "INNER JOIN FETCH user.authorities AS authorities " +
            "WHERE user.username = :username")
    User findByUsername(@Param("username") String username);
}