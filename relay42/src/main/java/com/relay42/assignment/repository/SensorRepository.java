package com.relay42.assignment.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import com.relay42.assignment.model.Sensor;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ibrahim AbdelHamid
 *
 */

@Repository
public interface SensorRepository extends ReactiveMongoRepository<Sensor, String>{

		
	/**
	 * @param pageable
	 * @return list of sensors paged
	 */
    @Query("{ id: { $exists: true }}")
	public Flux<Sensor> findAllSensorsPaged(Pageable pageable);
	
	/**
	 * @param sensorIds
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findBySensorIdIn(List<String> sensorIds, Pageable pageable );
	
	/**
	 * @param sensorId
	 * @return Mono<Sensor>
	 */
	public Flux<Sensor> findBySensorId(Sensor sensorId);
	
	
	/**
	 * @param sensorIds
	 * @param startDate
	 * @param endDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findBySensorIdInAndTimestampBetweenOrderByTimestamp(List<String> sensorIds,@Nullable Timestamp startDate ,@Nullable Timestamp endDate);
	
	
	/**
	 * @param sensorIds
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findBySensorIdIn(List<String> sensorIds);
	
	/**
	 * @param sensorIds
	 * @param startDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findBySensorIdInAndTimestampAfterOrderByTimestamp(List<String> sensorIds,@Nullable Timestamp startDate);
	
	/**
	 * @param sensorIds
	 * @param endDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findBySensorIdInAndTimestampBeforeOrderByTimestamp(List<String> sensorIds ,@Nullable Timestamp endDate);
	
	/**
	 * @param startDate
	 * @param endDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findByTimestampBetweenOrderByTimestamp(@Nullable Timestamp startDate ,@Nullable Timestamp endDate); 
	
	/**
	 * @param endDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findByTimestampBeforeOrderByTimestamp(@Nullable Timestamp endDate); 
	
	/**
	 * @param startDate
	 * @return Flux<Sensor>
	 */
	public Flux<Sensor> findByTimestampAfterOrderByTimestamp(@Nullable Timestamp startDate); 
	
	/**
	 * @param sensorIds
	 * @return count of the documents
	 */
	public Mono<Long> countBySensorIdIn(List<String> sensorIds);
	
}
