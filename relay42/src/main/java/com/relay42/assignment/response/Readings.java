/**
 * 
 */
package com.relay42.assignment.response;

import com.relay42.assignment.model.Sensor;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class Readings {
	
	private double average;
	private double median;
	private Sensor max;
	private Sensor min;
	
	/**
	 * @return the average
	 */
	public double getAverage() {
		return average;
	}
	/**
	 * @param average the average to set
	 */
	public void setAverage(double average) {
		this.average = average;
	}
	/**
	 * @return the median
	 */
	public double getMedian() {
		return median;
	}
	/**
	 * @param median the median to set
	 */
	public void setMedian(double median) {
		this.median = median;
	}
	/**
	 * @return the max
	 */
	public Sensor getMax() {
		return max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(Sensor max) {
		this.max = max;
	}
	/**
	 * @return the min
	 */
	public Sensor getMin() {
		return min;
	}
	/**
	 * @param min the min to set
	 */
	public void setMin(Sensor min) {
		this.min = min;
	}
	

}
