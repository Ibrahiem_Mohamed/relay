/**
 * @author Ibrahim AbdelHamid
 * 
 *         Contains All Models persisted to db including Mongodb or H2 (used for
 *         security Oauth)
 */
package com.relay42.assignment.model;