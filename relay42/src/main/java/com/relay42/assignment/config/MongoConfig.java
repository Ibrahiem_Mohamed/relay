/**
 * 
 */
package com.relay42.assignment.config;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@Configuration
public class MongoConfig {

	@Bean
	public MongoCustomConversions customConversions() {
		List<Converter<?, ?>> converters = new ArrayList<>();
		converters.add(DateWriterConverter.INSTANCE);
		converters.add(DateReaderConverter.INSTANCE);
		return new MongoCustomConversions(converters);
	}

	@WritingConverter
	enum DateWriterConverter implements Converter<Timestamp, Long> {

		INSTANCE;

		@Override
		public Long convert(Timestamp timeStamp) {
			return timeStamp.getTime();
		}
	}

	@ReadingConverter
	enum DateReaderConverter implements Converter<Long, Timestamp> {

		INSTANCE;

		@Override
		public Timestamp convert(Long date) {

			return new Timestamp(date);
		}
	}
}