/**
 * 
 */
package com.relay42.assignment.config.encryption;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Ibrahim AbdelHamid
 *
 *         For Injecting type of BCryptPasswordEncoder in PasswordEncoder bean
 */
@Configuration
public class Encoders {

	/**
	 * @return BCryptPasswordEncoder
	 */
	@Bean
	public PasswordEncoder oauthClientPasswordEncoder() {
		return new BCryptPasswordEncoder(4);
	}

	/**
	 * @return BCryptPasswordEncoder
	 */
	@Bean
	public PasswordEncoder userPasswordEncoder() {
		return new BCryptPasswordEncoder(8);
	}
}