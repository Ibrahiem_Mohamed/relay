/**
 * @author Ibrahim AbdelHamid
 * 
 *         Contains All Controller and endpoint mapping used in the app
 */
package com.relay42.assignment.controller;