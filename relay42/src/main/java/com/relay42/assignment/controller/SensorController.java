package com.relay42.assignment.controller;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.relay42.assignment.model.Sensor;
import com.relay42.assignment.response.Readings;
import com.relay42.assignment.service.SensorService;
import com.relay42.assignment.util.LinkBuilderUtil;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ibrahim AbdelHamid
 * 
 *         Contains endpoint for ... all sensors data or specific sensors data
 */
@RestController
public class SensorController {

	@Autowired
	private SensorService sensorService;

	/**
	 * @param pageable
	 * @param ids
	 * @param assembler
	 * @return Paged ResponseEntity<Sensor>
	 */
	@GetMapping(value = "api/v1/sensors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> getSensorsData(@PageableDefault(size = 500) Pageable pageable,
			@RequestParam(value = "id", required = false) String[] ids, PagedResourcesAssembler<Sensor> assembler) {

		Flux<Sensor> sensorsList;
		Page<Sensor> page;

		if (StringUtils.isEmpty(ids)) {
			sensorsList = sensorService.findAll(pageable);
			page = new PageImpl<>(sensorsList.collectList().block(), pageable,
					sensorService.SensorsDataCount().block());
		} else {
			sensorsList = sensorService.findSensorsByIds(Arrays.stream(ids).collect(Collectors.toList()), pageable);
			page = new PageImpl<>(sensorsList.collectList().block(), pageable,
					sensorService.SpesificSensorsDataCount(Arrays.asList(ids)).block());
		}

		return new ResponseEntity<>(
				assembler.toResource(page, LinkBuilderUtil.buildSelfLink(this.getClass(), pageable, ids, assembler)),
				HttpStatus.OK);

	}

	/**
	 * @param pageable
	 * @param ids
	 * @param start
	 * @param end
	 * @return Avg Reading Dto
	 */
	@GetMapping(value = "api/v1/sensors/average", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Mono<ResponseEntity<?>> getAvgSensorsData(@PageableDefault(size = 500) Pageable pageable,
			@RequestParam(value = "id", required = false) String[] ids,
			@RequestParam(value = "start", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date start,
			@RequestParam(value = "end", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date end) {

		Mono<Readings> readingsData;
		Timestamp startDate = start == null ? null : new Timestamp(start.getTime());
		Timestamp endDate = end == null ? null : new Timestamp(end.getTime());

		if (StringUtils.isEmpty(ids)) {
			readingsData = sensorService.getAverageReadings(startDate, endDate);
		} else {
			readingsData = sensorService.getAverageReadingsForSpesificSensors(Arrays.asList(ids), startDate,
					endDate);
		}

		return readingsData.map(data -> new ResponseEntity<>(data, HttpStatus.OK));

	}

}
