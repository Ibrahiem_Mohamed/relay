/**
 * @author Ibrahim AbdelHamid
 * 
 *         Contains Global Error Handling throw from service Layer
 */
package com.relay42.assignment.error;