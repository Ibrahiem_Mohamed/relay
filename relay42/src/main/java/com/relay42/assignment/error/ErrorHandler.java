package com.relay42.assignment.error;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.relay42.assignment.exception.NoDataFoundException;

/**
 * @author Ibrahim AbdelHamid Global Error Handler for different exceptions
 */
@ControllerAdvice
public class ErrorHandler {

	private final Logger logger = LoggerFactory.getLogger(ErrorHandler.class);

	/**
	 * @param request
	 * @param ex      returned 204 HTTP status with reason
	 * @return
	 */
	@ExceptionHandler(NoDataFoundException.class)
	@ResponseBody
	public ResponseEntity<ErrorObject> handleNoDataFoundException(HttpServletRequest request, Exception ex) {
		logger.error("NoDataFoundException : " + ex.getLocalizedMessage());

		return new ResponseEntity<ErrorObject>(new ErrorObject(new Date(), "No Matched Data Found For the Params"),
				HttpStatus.NOT_FOUND);

	}

}
