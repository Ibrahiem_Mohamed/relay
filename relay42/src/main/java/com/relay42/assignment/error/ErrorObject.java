package com.relay42.assignment.error;

import java.util.Date;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public class ErrorObject {
	
	private Date timeStamp;
	private String message;
	/**
	 * @return the timeStamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @param timeStamp
	 * @param message
	 */
	public ErrorObject(Date timeStamp, String message) {
		super();
		this.timeStamp = timeStamp;
		this.message = message;
	}
	

}
