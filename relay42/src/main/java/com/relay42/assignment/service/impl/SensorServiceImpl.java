package com.relay42.assignment.service.impl;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.relay42.assignment.exception.NoDataFoundException;
import com.relay42.assignment.model.Sensor;
import com.relay42.assignment.repository.SensorRepository;
import com.relay42.assignment.response.Readings;
import com.relay42.assignment.service.SensorService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ibrahim AbdelHamid
 *
 */
@Service
public class SensorServiceImpl implements SensorService {

	@Autowired
	SensorRepository sensorRepo;

	@Override
	public Flux<Sensor> findSensorById(Sensor sensor) {
		return sensorRepo.findBySensorId(sensor);
	}

	@Override
	public Flux<Sensor> findSensorsByIds(List<String> sensorsIds, Pageable pageable) {
		return sensorRepo.findBySensorIdIn(sensorsIds, pageable);
	}

	@Override
	public Flux<Sensor> findAll(Pageable pageable) {
		return sensorRepo.findAllSensorsPaged(pageable);
	}

	@Override
	public Mono<Long> SensorsDataCount() {
		return sensorRepo.count();
	}

	@Override
	public Mono<Readings> getAverageReadings(Timestamp startDate, Timestamp endDate) {

		List<Sensor> matchedSensors;

		if (startDate == null && endDate == null) {
			matchedSensors = sensorRepo.findAll().toStream().collect(Collectors.toList());
		} else if (startDate == null) {
			matchedSensors = sensorRepo.findByTimestampBeforeOrderByTimestamp(endDate).toStream()
					.collect(Collectors.toList());
		} else if (endDate == null) {
			matchedSensors = sensorRepo.findByTimestampAfterOrderByTimestamp(startDate).toStream()
					.collect(Collectors.toList());
		} else {
			matchedSensors = sensorRepo.findByTimestampBetweenOrderByTimestamp(startDate, endDate).toStream()
					.collect(Collectors.toList());

		}

		return calculateReadingsData(matchedSensors);
	}

	@Override
	public Mono<Readings> getAverageReadingsForSpesificSensors(List<String> ids, Timestamp startDate, Timestamp endDate) {

		List<Sensor> matchedSensors;

		if (startDate == null && endDate == null) {
			matchedSensors = sensorRepo.findBySensorIdIn(ids).toStream().collect(Collectors.toList());
		} else if (startDate == null) {
			matchedSensors = sensorRepo.findBySensorIdInAndTimestampBeforeOrderByTimestamp(ids, endDate).toStream()
					.collect(Collectors.toList());
		} else if (endDate == null) {
			matchedSensors = sensorRepo.findBySensorIdInAndTimestampAfterOrderByTimestamp(ids, startDate).toStream()
					.collect(Collectors.toList());
		} else {
			matchedSensors = sensorRepo.findBySensorIdInAndTimestampBetweenOrderByTimestamp(ids, startDate, endDate)
					.toStream().collect(Collectors.toList());
		}

		return calculateReadingsData(matchedSensors);
	}

	@Override
	public Mono<Long> SpesificSensorsDataCount(List<String> sensorIds) {

		return sensorRepo.countBySensorIdIn(sensorIds);
	}

	/**
	 * @param matchedSensors
	 * @return Readings object
	 */
	private Mono<Readings> calculateReadingsData(List<Sensor> matchedSensors) {

		Readings readings = new Readings();

		if (matchedSensors == null || matchedSensors.isEmpty())
			throw new NoDataFoundException();

		Supplier<Stream<Sensor>> streamSupplier = () -> matchedSensors.stream();

		// setting the average readings
		streamSupplier.get().mapToDouble(Sensor::getTemperature).average()
				.ifPresent(average -> readings.setAverage(average));

		// setting the median readings
		Supplier<DoubleStream> sortedTemperature = () -> streamSupplier.get().mapToDouble(Sensor::getTemperature)
				.sorted();

		double median = sortedTemperature.get().count() % 2 == 0
				? sortedTemperature.get().skip(sortedTemperature.get().count() / 2 - 1).limit(2).average().getAsDouble()
				: sortedTemperature.get().skip(sortedTemperature.get().count() / 2).findFirst().getAsDouble();

		readings.setMedian(median);
		// setting the max sensor based on temperature value
		streamSupplier.get().max(Comparator.comparing(Sensor::getTemperature)).ifPresent(max -> readings.setMax(max));

		// setting the min sensor based on temperature value
		streamSupplier.get().min(Comparator.comparing(Sensor::getTemperature)).ifPresent(min -> readings.setMin(min));

		return Mono.just(readings);

	}
}
