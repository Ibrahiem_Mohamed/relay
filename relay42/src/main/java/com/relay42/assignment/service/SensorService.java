package com.relay42.assignment.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.relay42.assignment.model.Sensor;
import com.relay42.assignment.response.Readings;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Ibrahim AbdelHamid
 *
 */
public interface SensorService {

	
	/**
	 * @return count of all sensors documents
	 */
	Mono<Long>SensorsDataCount();
	
	
	/**
	 * @return count of spesific sensors documents
	 */
	Mono<Long> SpesificSensorsDataCount(List<String> sensorIds);
	
	/**
	 * @param pageable
	 * @return
	 */
	Flux<Sensor> findAll(Pageable pageable);
	
	
	/**
	 * @param sensor
	 * @return list of matched sensor data
	 */
	Flux<Sensor> findSensorById(Sensor sensor);
	
	/**
	 * @param sensors
	 * @param pageable
	 * @return list of sensors
	 */
	Flux<Sensor> findSensorsByIds(List<String>	sensors , Pageable pageable);
	
	
	/**
	 * @param startDate
	 * @param endDate
	 * @return Readings objects
	 */
	Mono <Readings> getAverageReadings(Timestamp startDate , Timestamp endDate);
	
	/**
	 * @param ids
	 * @param startDate
	 * @param endDate
	 * @return Readings objects
	 */
	Mono <Readings> getAverageReadingsForSpesificSensors(List<String> ids,Timestamp startDate , Timestamp endDate);
	
}
