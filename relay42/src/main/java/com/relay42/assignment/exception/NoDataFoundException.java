package com.relay42.assignment.exception;

/**
 * @author Ibrahim AbdelHamid
 * 
 *         throw when no matched records found in the db
 */
public class NoDataFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
