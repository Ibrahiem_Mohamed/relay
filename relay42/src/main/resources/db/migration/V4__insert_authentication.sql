INSERT INTO OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
	VALUES ('relay42-oauth2-read-client', 'resource-server-rest-api',
	/*relay42*/'$2a$04$yrGauv2GSEEpefhw4hseeOUarB4pw8bZUiNTXMhpw4ifmy.mP/xLy',
	'read', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);

INSERT INTO OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
	VALUES ('relay42-oauth2-read-write-client', 'resource-server-rest-api',
	/*relay42*/'$2a$04$yrGauv2GSEEpefhw4hseeOUarB4pw8bZUiNTXMhpw4ifmy.mP/xLy',
	'read,write', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);
